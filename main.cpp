#include "stupid_header.h"
#include "syntax.h"
#include "parse.h"
#include "polish.h"
#include "table.h"
enum {ip_arg = 1, port_arg = 2, nick_arg = 3, progfile_arg = 4,
    num_correct_par = 5
};

const char *magic_bot_str = "## ";
const char *magic_start_str = "GAME";

int main(int argc, char **argv)
{
    signal(SIGPIPE, SIG_IGN);
    struct sockaddr_in serv_addr;
    int serv_port;
    serv_addr.sin_family = AF_INET;
    if (argc != num_correct_par ||
        sscanf(argv[port_arg], "%d\n", &serv_port) != 1 ||
        !inet_aton(argv[ip_arg], &(serv_addr.sin_addr)))
    {
        printf("ManagerBot usage: [ip] [port] [bots nick] [bot program]\n");
        return 1;
    }
    int scriptfd;
    if ((scriptfd = open(argv[progfile_arg], O_RDONLY)) == -1) {
        printf("Can't open file containing bot script\n");
        perror("open");
        return 1;
    }
    Table table;
    PolishList polishlist;
    SyntaxAnalysator sa(scriptfd, table, polishlist);
    if (sa.StartSyntAn()) {
        return 1;
    }
    #ifdef DEBUG
    printf("Printing PolishList...\n");
    polishlist.PrintList();
    #endif
    serv_addr.sin_port = htons(serv_port);
    int c_socket;
    if ((c_socket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        return 1;
    }
    if (connect(c_socket, (struct sockaddr *)&serv_addr, sizeof(serv_addr))) {
        perror("connect");
        return 1;
    }
    fd_set readfds;
    bool is_nick = 0;
    WrapWL ww;
    while (1) {
        FD_ZERO(&readfds);
        FD_SET(c_socket, &readfds);
        if (select(c_socket + 1, &readfds, 0, 0, 0) < 1) {
            perror("select");
            continue;
        }
        if (FD_ISSET(c_socket, &readfds)) {
            do {
                ww.refresh(c_socket);
                if (ww.is_closed) {
                    printf("Connection lost\n");
                    return 0;
                } else if (ww.is_ready) {
                    if (!is_nick) {
                        String s = magic_bot_str;
                        s  = s + argv[nick_arg] + "\n";
                        s.send(c_socket);
                        is_nick = 1;
                    }
                    #ifdef DEBUG
                    printf("Received from server:\n");
                    #endif
                    ww.print();
                    if (ww[0] == magic_start_str)
                        goto startgame;
                }
            } while (ww.is_buffered);
        }
    }
    startgame:
    printf("Starting game...\n");
    polishlist.RunPolish(c_socket, ww);
    close(scriptfd);
    return 0;
}
