#include "strings.h"

int String::send(int fd) const
{
    return write(fd, s, sz);
}

String String::operator+(const String &b) const
{
    String conc(0);
    if ((conc.sz = sz + b.sz) > 0) {
        conc.s = new char [conc.sz];
        int i;
        for (i = 0; i < sz - 1; i++)
            conc.s[i] = s[i];
        for (int j = 0; i < conc.sz - 1; i++, j++)
            conc.s[i] = b.s[j];
        conc.s[i] = '\0';
    }
    return conc;
}

const char *String::GetStr() const
{
    return s;
}

String::String(const char *str)
: s(0), sz(0)
{
    if (str) {
        int i;
        for (i = 0; str[i] != '\0'; i++)
        {}
        sz = ++i;
        s = new char [sz];
        for (i = 0; i < sz; i++)
            s[i] = str[i];
    }
}

String::String(const String &b)
: s(0), sz(0)
{
    if (b.sz) {
        sz = b.sz;
        s = new char [sz];
        for (int i = 0; i < sz; i++)
            s[i] = b.s[i];
    }
}

String & String::operator=(const String &b)
{
    if (this == &b) {
        return *this;
    }
    if (s)
        delete [] s;
    if ((sz = b.sz) != 0) {
        s = new char [sz];
        for (int i = 0; i < sz; i++)
            s[i] = b.s[i];
    }
    return *this;
}

int String::size() const
{
    return sz;
}

bool String::operator==(const String &b) const
{
    for (int i = 0; s[i] == b.s[i]; i++) {
        if (s[i] == '\0')
            return true;
    }
    return false;
}

bool String::operator!=(const String &b) const
{
    for (int i = 0; s[i] == b.s[i]; i++) {
        if (s[i] == '\0')
            return false;
    }
    return true;
}

bool isspace2(const char &c)
{
    if (c == '\n' || c == ' ' || c == '\f' || c == '\r' || c == '\t'
        || c == '\v')
        return true;
    else
        return false;
}

