#ifndef __BOTFUNC_H__
#define __BOTFUNC_H__
#include "stupid_header.h"
#include "parse.h"

class GameInfo
{
    enum {p_hash, p_month, p_is_ta, p_bot_i, p_active_pl, p_max_pl,
        p_rw_q, p_rw_p, p_goods_q, p_goods_p, p_cost_raw, p_cost_good,
        p_cost_fact, p_unit_fact, p_unit_prod};
    enum {id_, money_, rw_, goods_, factories_, produced_,
        sold_goods_q_, sold_goods_p_, bou_rw_q_, bou_rw_p_};

    enum {offset = 11, startoffset = 16};
    struct PlayerInfo
    {
         int id, money, rw, goods, factories, produced,
            sold_goods_q, sold_goods_p, bou_rw_q, bou_rw_p;
    };
    WrapWL &ww;
    int fdd;
    int month;
    int is_ta;
    int bot_i;
    int active_pl;
    int max_pl;
    int rw_q, rw_p, goods_q, goods_p;
    int cost_good, cost_fact, cost_raw;
    int unit_prod, unit_fact;
    PlayerInfo *plinfo;
    int iter;
public:
    void GetServerInfo();
    ~GameInfo()
    {
        if (plinfo)
            delete [] plinfo;
    }
    GameInfo(int d, WrapWL &ii)
    : ww(ii), fdd(d), plinfo(0)
    {}
    void buy(int quant, int price); //buy raw_mat
    void sell(int quant, int price); //sell goods
    void prod(int q); //product some goods
    void build(int q); // build factories
    void endturn();

    int is_turn_available(); //return true if bot can still plays
    int my_id();// my player number
    int turn(); // current month
    int players(); //number of all players
    int active_players(); //active players
    int supply(); //num of raw
    int raw_price(); //min price of raw
    int demand(); // num of goods
    int production_price(); //max price of good
    int get_id(); //iterator of players id
    int money(int id); // player's money
    int raw(int id);
    int production(int id);
    int factories(int id);
    int manufactured(int id);
    int result_prod_bought(int id);
    int result_prod_price(int id);
    int result_raw_bought(int id);
    int result_raw_price(int id);
    int get_cost_good();
    int get_cost_fact();
    int get_cost_raw();
    int get_unit_prod();
    int get_unit_fact();
};
#endif
