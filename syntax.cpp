#include "syntax.h"
void LexError::PrintMsg()
{
    printf("LexError ");
}

void ExprError::PrintMsg()
{
    printf("ExpressionError ");
}

void GarbageSymError::PrintMsg()
{
    printf("GarbageSymbol ");
}

void MatchNameError::PrintMsg()
{
    printf("MatchNameError ");
}

void MatchStrError::PrintMsg()
{
    printf("MatchStrError ");
}

void SyntaxAnalysator::addUserID()
{
    Payload *pp = tbl.userid[tkn.i_table].p;
    switch (pp->type) {
    case def_afloat: {
        IdaFloat *k = static_cast<IdaFloat*>(pp);
        polishlist.add(new PolishFloat(k->p));
        polishlist.add(new PolishInt(&(k->sz)));
        break; }
    case def_aint: {
        IdaInt *k = static_cast<IdaInt*>(pp);
        polishlist.add(new PolishInt(k->p));
        polishlist.add(new PolishInt(&(k->sz)));
        break; }
    case def_mfloat: {
        IdmFloat *ff = static_cast<IdmFloat*>(pp);
        polishlist.add(new PolishFloat(&(ff->f)));
        break; }
    default: {
        IdmInt *ii = static_cast<IdmInt*>(pp);
        polishlist.add(new PolishInt(&(ii->i)));
        break; }
    }
}

void SyntaxAnalysator::addInt()
{
    PolishInt *k = new PolishInt(&tbl.int_table[tkn.i_table]);
    polishlist.add(k);
}

void SyntaxAnalysator::addFloat()
{
    PolishFloat *k = new PolishFloat(&tbl.float_table[tkn.i_table]);
    polishlist.add(k);
}

void SyntaxAnalysator::addString()
{
    PolishString *ps = new PolishString(&tbl.str_table[tkn.i_table]);
    polishlist.add(ps);
}
void SyntaxAnalysator::addNopar()
{
    switch (tkn.i_table) {
    case ista:
        polishlist.add<PolishIsTurnAvailable>();
        break;
    case rawprice:
        polishlist.add<PolishRawPrice>();
        break;
    case productprice:
        polishlist.add<PolishProductPrice>();
        break;
    case tturn:
        polishlist.add<PolishTurn>();
        break;
    case myid:
        polishlist.add<PolishMyID>();
        break;
    case pplayers:
        polishlist.add<PolishPlayers>();
        break;
    case activeplayers:
        polishlist.add<PolishActivePlayers>();
        break;
    case ssupply:
        polishlist.add<PolishSupply>();
        break;
    case ddemand:
        polishlist.add<PolishDemand>();
        break;
    case getid:
        polishlist.add<PolishGetID>();
        break;
    case costgood:
        polishlist.add<PolishCostGood>();
        break;
    case costfact:
        polishlist.add<PolishCostFact>();
        break;
    case costraw:
        polishlist.add<PolishCostRaw>();
        break;
    case unitprod:
        polishlist.add<PolishUnitProd>();
        break;
    case unitfact:
        polishlist.add<PolishUnitFact>();
        break;
    default:
        break;
    }
}

void SyntaxAnalysator::addOnepar()
{
    switch (tkn.i_table) {
    case mmoney:
        polishlist.add<PolishMoney>();
        break;
    case rraw:
        polishlist.add<PolishRaw>();
        break;
    case pproduction:
        polishlist.add<PolishProduction>();
        break;
    case ffactories:
        polishlist.add<PolishFactories>();
        break;
    case mmanuf:
        polishlist.add<PolishManufactured>();
        break;
    case tradesprodbought:
        polishlist.add<PolishTradesProdBought>();
        break;
    case tradesprodprice:
        polishlist.add<PolishTradesProdPrice>();
        break;
    case tradesrawbought:
        polishlist.add<PolishTradesRawBought>();
        break;
    case tradesrawprice:
        polishlist.add<PolishTradesRawPrice>();
        break;
    default:
        break;
    }
}

SyntaxAnalysator::SyntaxAnalysator(int sscriptfd, Table &t, PolishList &pl)
: tbl(t), la(&t), polishlist(pl), scriptfd(sscriptfd)
{
    tkn = Token(_notready, 0, 0);
}
SyntaxAnalysator::~SyntaxAnalysator()
{}

void SyntaxAnalysator::Expr()
{
//it should save last ";"!!!! then S() will check it
    A();
    while (CheckStr("|")) {
        NextToken();
        A();
        polishlist.add<PolishOr>();
    }
}

void SyntaxAnalysator::A()
{
    B();
    while (CheckStr("&")) {
        NextToken();
        B();
        polishlist.add<PolishAnd>();
    }
}

void SyntaxAnalysator::B()
{
    if (CheckStr("!")) {
        NextToken(); Expr();
        polishlist.add<PolishNot>();
    } else {
        C();
    }
}
void SyntaxAnalysator::C()
{
    D();
    while (1) {
        if (CheckStr("!=")) {
            NextToken();
            D();
            polishlist.add<PolishNotEqual>();
        } else if (CheckStr("=")) {
            NextToken();
            D();
            polishlist.add<PolishEqual>();
        } else if (CheckStr(">")) {
            NextToken();
            D();
            polishlist.add<PolishGreater>();
        } else if (CheckStr(">=")) {
            NextToken();
            D();
            polishlist.add<PolishGreaterEq>();
        } else if (CheckStr("<")) {
            NextToken();
            D();
            polishlist.add<PolishLess>();
        } else if (CheckStr("<=")) {
            NextToken();
            D();
            polishlist.add<PolishLessEq>();
        } else {
            break;
        }
    }
}

void SyntaxAnalysator::D()
{
    E();
    while (1) {
        if (CheckStr("-")) {
            NextToken();
            E();
            polishlist.add<PolishSub>();
        } else if (CheckStr("+")) {
            NextToken();
            E();
            polishlist.add<PolishSum>();
        } else {
            break;
        }
    }
}

void SyntaxAnalysator::E()
{
    F();
    while (1) {
        if (CheckStr("*")) {
            NextToken();
            F();
            polishlist.add<PolishMul>();
        } else if (CheckStr("/")) {
            NextToken();
            F();
            polishlist.add<PolishDiv>();
        } else {
            break;
        }
    }
}

void SyntaxAnalysator::F()
{
    G();
    while (1) {
        if (CheckStr("%")) {
            NextToken();
            G();
            polishlist.add<PolishMod>();
        } else {
            break;
        }
    }
}

void SyntaxAnalysator::G()
{
    if (CheckStr("-")) {
        NextToken();
        H();
        polishlist.add<PolishMinus>();
    } else {
        H();
    }
}

void SyntaxAnalysator::H()
{
    if (CheckStr("(")) {
        NextToken();
        Expr();
        MatchStr(")"); NextToken();
    } else {
        if (CheckName(_idm)) {
            tbl.check_userid(tkn.i_table);
            addUserID();
            NextToken();
        } else if (CheckName(_ida)) {
            tbl.check_userid(tkn.i_table);
            addUserID();
            NextToken(); MatchStr("[");
            NextToken(); Expr();
            MatchStr("]"); NextToken();
            polishlist.add<PolishBrackets>();
        } else if (CheckName(_floating)) {
            addFloat();
            NextToken();
        } else if (CheckName(_integer)) {
            addInt();
            NextToken();
        } else if (CheckName(_onepar)) {
            int tmp_tt = tkn.i_table;
            NextToken(); MatchStr("(");
            NextToken(); Expr();
            MatchStr(")");
            tkn.i_table = tmp_tt;
            addOnepar();
            NextToken();
        } else if (CheckName(_nopar)) {
            addNopar();
            NextToken();
        } else {
            throw ExprError();
        }
    }
}

void SyntaxAnalysator::S_goto()
{
    NextToken(); MatchName(_label);
    tbl.define_goto_label(tkn.i_table, polishlist);
    NextToken(); MatchStr(";");
    polishlist.add<PolishGoto>();
}

void SyntaxAnalysator::S_if()
{
    PolishAtom ***pa = new PolishAtom**;
    polishlist.add(new PolishLabelSD(pa));
    NextToken(); Expr();
    polishlist.add<PolishGotoFalse>();
    MatchStr("{");
    NextToken(); S();
    MatchStr("}");
    NextToken();
    if (CheckStr("else")) {
        PolishAtom ***pendif = new PolishAtom**;
        polishlist.add(new PolishLabelSD(pendif));
        polishlist.add<PolishGoto>();
        *pa = polishlist.GetLast();
        NextToken(); MatchStr("{");
        NextToken(); S();
        MatchStr("}");
        *pendif = polishlist.GetLast();
        NextToken(); S();
    } else {
        *pa = polishlist.GetLast();
        S();
    }
}
void SyntaxAnalysator::S_while()
{
    PolishAtom ***pend = new PolishAtom**, ***pstart = new PolishAtom**;
    *pstart = polishlist.GetLast();
    polishlist.add(new PolishLabelSD(pend));
    NextToken(); Expr();
    polishlist.add<PolishGotoFalse>();
    MatchStr("{");
    NextToken(); S();
    MatchStr("}");
    polishlist.add(new PolishLabelSD(pstart));
    polishlist.add<PolishGoto>();
    *pend = polishlist.GetLast();
    //NextToken(); S();
}
void SyntaxAnalysator::S_id_def_fun(int dt)
{
    NextToken();
    if (CheckName(_idm)) {
        tbl.define_userid(tkn.i_table, dt);
    } else {
        MatchName(_ida);
        int ida_itable = tkn.i_table;
        NextToken();
        MatchStr("["); NextToken();
        MatchName(_integer);
        tbl.define_userid(ida_itable, dt+1, tbl.int_table[tkn.i_table]);
        NextToken(); MatchStr("]");
    }
    NextToken();
}
void SyntaxAnalysator::S_id_def()
{
    int dt;
    if (CheckStr("int"))
        dt = def_mint;
    else
        dt = def_mfloat;
    S_id_def_fun(dt);
    while (1) {
        if (CheckStr(","))
            S_id_def_fun(dt);
        else
            break;
    }
}
void SyntaxAnalysator::S_print()
{
    polishlist.add<PolishStop>();
    NextToken();
    if (CheckName(_str)) {
        addString();
        NextToken();
    } else {
        Expr();
    }
    while (1) {
        if (CheckStr(",")) {
            NextToken();
            if (CheckName(_str)) {
                addString();
                NextToken();
            } else {
                Expr();
            }
        } else {
            break;
        }
    }
    polishlist.add<PolishPrint>();
}
void SyntaxAnalysator::S_is()
{
    if (CheckName(_ida)) {
        tbl.check_userid(tkn.i_table);
        addUserID();
        NextToken(); MatchStr("[");
        NextToken(); Expr();
        MatchStr("]");
        polishlist.add<PolishBrackets>();
    } else {
        tbl.check_userid(tkn.i_table);
        addUserID();
    }
    NextToken(); MatchStr(":=");
    NextToken(); Expr();
    MatchStr(";");
    polishlist.add<PolishIs>();
    NextToken(); S();
}
void SyntaxAnalysator::S()
{
    if (CheckName(_keywords)) {
        if (CheckStr("goto")) {
            S_goto();
        } else if (CheckStr("if")) {
            S_if();
            return;
        } else if (CheckStr("while")) {
            S_while();
        } else if (CheckStr("int") || CheckStr("float")) {
            S_id_def();
            MatchStr(";");
        } else if (CheckStr("print")) {
            S_print();
            MatchStr(";");
        } else if (CheckStr("buy")) {
            NextToken(); Expr();
            MatchStr(",");
            NextToken(); Expr();
            polishlist.add<PolishBuy>();
            MatchStr(";");
        } else if (CheckStr("sell")) {
            NextToken(); Expr();
            MatchStr(",");
            NextToken(); Expr();
            polishlist.add<PolishSell>();
            MatchStr(";");
        } else if (CheckStr("prod")) {
            NextToken(); Expr();
            polishlist.add<PolishProd>();
            MatchStr(";");
        } else if (CheckStr("build")) {
            NextToken(); Expr();
            polishlist.add<PolishBuild>();
            MatchStr(";");
        } else {
            polishlist.add<PolishEndturn>();
            NextToken();
            MatchStr(";");
        }
        NextToken(); S();
    } else if (CheckName(_end)) {
        return;
    } else if (CheckName(_label)) {
        tbl.define_label_jump(tkn.i_table, polishlist);
        NextToken(); MatchStr(";");
        NextToken(); S();
    } else if (CheckName(_ida) || CheckName(_idm)) {
        S_is();
    }
}

void SyntaxAnalysator::NextToken()
{
    if (tkn.name == _end) {
        return;
    }
    tkn.name = _notready;
    while (tkn.name == _notready) {
        if (la.Gt(c, tkn)) {
            if (read(scriptfd, &c, sizeof(c)) != 1)
                tkn = Token(_end, 0, tkn.str_num);
        }
    }
    #ifdef DEBUG2
    if (tkn.name == _serv) {
        printf("SERV TOKEN %s\n", serv_table[tkn.i_table].s);
    }
    #endif
    if (tkn.name == _error) {
        throw LexError();
    }
}
void SyntaxAnalysator::MatchStr(const String &s)
{
    #ifdef DEBUG2
    printf("MATCHING %s with ", s.s);
    #endif
    switch (tkn.name) {
    case _keywords:
        #ifdef DEBUG2
        printf("%s \n", keyw_table[tkn.i_table].s);
        #endif
        if (keyw_table[tkn.i_table] == s)
            return;
        break;
    case _serv:
        #ifdef DEBUG2
        printf("%s \n", serv_table[tkn.i_table].s);
        #endif
        if (serv_table[tkn.i_table] == s)
            return;
        break;
    case _nopar:
        #ifdef DEBUG2
        printf("%s \n", func_nopar[tkn.i_table].s);
        #endif
        if (func_nopar[tkn.i_table] == s)
            return;
        break;
    case _onepar:
        #ifdef DEBUG2
        printf("%s \n", func_onepar[tkn.i_table].s);
        #endif
        if (func_onepar[tkn.i_table] == s)
            return;
        break;
    default:
        break;
    }
    printf("[Error] Expected %s\n", s.s);
    throw MatchStrError();
}

void SyntaxAnalysator::MatchName(TokenName tn)
{
    if (tkn.name != tn) {
        printf("[Error] Expected ");
        switch (tn) {
        case _idm: printf("ident_mono\n"); break;
        case _ida: printf("ident_array\n"); break;
        case _str: printf("string\n"); break;
        case _keywords: printf("keyword\n"); break;
        case _integer: printf("integer\n"); break;
        case _floating: printf("floating\n"); break;
        case _label: printf("label\n"); break;
        case _serv: printf("service char\n"); break;
        case _onepar: printf("one-param function\n"); break;
        case _nopar: printf("no-param function\n"); break;
        default: break;
        };
        throw MatchNameError();
    }
}
bool SyntaxAnalysator::CheckStr(const String &s)
{
    switch (tkn.name) {
    case _keywords:
        if (keyw_table[tkn.i_table] == s)
            return 1;
        break;
    case _serv:
        if (serv_table[tkn.i_table] == s)
            return 1;
        break;
    case _nopar:
        if (func_nopar[tkn.i_table] == s)
            return 1;
        break;
    case _onepar:
        if (func_onepar[tkn.i_table] == s)
            return 1;
        break;
    default:
        break;
    }
    return 0;
}

bool SyntaxAnalysator::CheckName(TokenName tn)
{
    if (tkn.name != tn)
        return 0;
    return 1;
}
int SyntaxAnalysator::StartSyntAn()
{
    try {
        if (read(scriptfd, &c, sizeof(c)) != 1) {
            printf("Empty file\n");
            return -1;
        }
        NextToken();
        S();
        if (tkn.name != _end)
            throw GarbageSymError();
        #ifdef DEBUG
        printf("SyntaxAnalys: no errors\n");
        #endif
        return 0;
    } catch (Error &e) {
        e.PrintMsg();
        printf("on %d\n", tkn.str_num);
        return -1;
    }
}

