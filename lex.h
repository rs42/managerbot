#ifndef __LEX_H__
#define __LEX_H__
#include "stupid_header.h"
#include "table.h"

class LexAnalysator
{
    enum lex_state
    {
        s_start, s_integer, s_float, s_label1, s_label2,
        s_str, s_id1, s_id2, s_key, s_is, s_not, s_le, s_ge,
    };
    Table *tbl;
    float f_buf;
    int f_point;
    bool token_ready;
    bool need_new;
    int str_num;
    lex_state state;
    int int_buf;
    char cbuf[bufsz + 1];
    int cbuf_n;
    char c;
    Token t;
    void S_start();
    void S_integer();
    void S_float();
    void S_label1();
    void S_label2();
    void S_str();
    void S_id1();
    void S_id2();
    void S_key();
    void S_is();
    void S_not();
    void S_le();
    void S_ge();
    Token GetToken0();
    Token GetToken1(char cc);
    Token Foo();
public:
    bool Gt(char cc, Token &tkn);
    LexAnalysator(Table *tb)
    : tbl(tb), f_buf(0), f_point(start_f_point),
    token_ready(0), need_new(1), str_num(1), state(s_start),
    int_buf(0), cbuf_n(0)
    {}
};
#endif
