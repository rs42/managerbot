#include "lex.h"

bool is_incharset(const char *s, char c)
{
    for (int i = 0; s[i] != '\0'; i++) {
        if (c == s[i])
            return true;
    }
    return false;
}

int is_instrset(const String *sset, const String &s)
{
    for (int i = 0; sset[i] != ""; i++) {
        if (sset[i] == s)
            return i;
    }
    return -1;
}

int char_to_num(char c)
{
    if (c - '0' >= 0 && '9' - c >= 0) {
        return c - '0';
    }
    return -1;
}

bool is_lett(char c)
{
    if ((c - 'a' >= 0 && 'z' - c >= 0) ||
        (c - 'A' >= 0 && 'Z' - c >= 0))
    {
        return true;
    }
    return false;
}

void LexAnalysator::S_not()
{
    state = s_start;
    token_ready = true;
    if (c == '=') {
        need_new = true;
        t = Token(_serv, p_noteq, str_num);
    } else {
        need_new = false;
        t = Token(_serv, p_notb, str_num);
    }
}
void LexAnalysator::S_le()
{
    state = s_start;
    token_ready = true;
    if (c == '=') {
        need_new = true;
        t = Token(_serv, p_leq, str_num);
    } else {
        need_new = false;
        t = Token(_serv, p_less, str_num);
    }
}
void LexAnalysator::S_ge()
{
    state = s_start;
    token_ready = true;
    if (c == '=') {
        need_new = true;
        t = Token(_serv, p_geq, str_num);
    } else {
        need_new = false;
        t = Token(_serv, p_great, str_num);
    }

}
void LexAnalysator::S_float()
{
    int a;
    if ((a = char_to_num(c)) != -1) {
        need_new = true;
        if (f_point > max_f_point) {
            //error float overflow
            token_ready = true;
            t = Token(_error, 0, str_num);
        }
        f_buf += (a*1.0)/f_point;
        f_point*=10;
    } else if (is_incharset(SpaceDiv, c) ||
            is_incharset(NonSpaceDiv, c))
    {
        state = s_start;
        token_ready = true;
        need_new = false;
        int pr = tbl->add_float(f_buf);
        if (pr == -1) {
            //error no place in table
            t = Token(_error, 0, str_num);
        } else {
            t = Token(_floating, pr, str_num);
        }
    } else {
        token_ready = true;
        t = Token(_error, 0, str_num);
    }
}

void LexAnalysator::S_id1()
{
    need_new = true;
    if (is_lett(c)) {
        token_ready = false;
        cbuf[cbuf_n++] = c;
        state = s_id2;
    } else {
        //error wrong id, it should start with letter
        token_ready = true;
        t = Token(_error, 0, str_num);
    }
}
void LexAnalysator::S_id2()
{
    if (cbuf_n == bufsz) {
        //too long name
        token_ready = true;
        t = Token(_error, 0, str_num);
    } else if (c == '[') {
        need_new = false;
        state = s_start;
        token_ready = true;
        cbuf[cbuf_n++] = '\0';
        int pr = tbl->add_userid(cbuf);
        if (pr == -1) {
            //error no place in table
            t = Token(_error, 0, str_num);
        } else {
            t = Token(_ida, pr, str_num);
        }
    } else if (is_lett(c) || (char_to_num(c)) != -1) {
        cbuf[cbuf_n++] = c;
        need_new = true;
    } else if (is_incharset(SpaceDiv, c) ||
            is_incharset(NonSpaceDiv, c))
    {
        state = s_start;
        need_new = false;
        token_ready = true;
        cbuf[cbuf_n++] = '\0';
        int pr = tbl->add_userid(cbuf);
        if (pr == -1) {
            //error no place in table
            t = Token(_error, 0, str_num);
        } else {
            t = Token(_idm, pr, str_num);
        }
    } else {
        token_ready = true;
        t = Token(_error, 0, str_num);
    }
}


void LexAnalysator::S_integer()
{
    int a;
    if (c == '.') {
    //oh boy float here we go
        need_new = true;
        token_ready = false;
        state = s_float;
        f_buf = int_buf;
        f_point = start_f_point;
    } else if ((a = char_to_num(c)) == -1) {
        token_ready = true;
        if (is_incharset(SpaceDiv, c) ||
            is_incharset(NonSpaceDiv, c))
        {
            state = s_start;
            need_new = false;
            int pr = tbl->add_int(int_buf);
            if (pr == -1) {
                //error no place in table
                t = Token(_error, 0, str_num);
            } else {
                t = Token(_integer, pr, str_num);
            }
        } else {
            t = Token(_error, 0, str_num);
        }
    } else {
        //ok stay here
        //maybe overflow???
        need_new = true;
        int_buf = 10 * int_buf + a;
    }
}
void LexAnalysator::S_is()
{
    token_ready = true;
    need_new = true;
    if (c == '=') {
        state = s_start;
        t = Token(_serv, p_is, str_num);
    } else {
        t = Token(_error, 0, str_num);
    }
}
void LexAnalysator::S_key()
{
    if (cbuf_n == bufsz) {
        token_ready = true;
        t = Token(_error, 0, str_num);
    } else {
        if (is_incharset(SpaceDiv, c) ||
            is_incharset(NonSpaceDiv, c))
        {
            state = s_start;
            token_ready = true;
            need_new = false;
            cbuf[cbuf_n++] = '\0';
            int pr_keyw = is_instrset(keyw_table, cbuf);
            int pr_onepar = is_instrset(func_onepar, cbuf);
            int pr_nopar = is_instrset(func_nopar, cbuf);
            if (pr_keyw != -1)
                t = Token(_keywords, pr_keyw, str_num);
            else if (pr_nopar != -1)
                t = Token(_nopar, pr_nopar, str_num);
            else if (pr_onepar != -1)
                t = Token(_onepar, pr_onepar, str_num);
            else
                t = Token(_error, 0, str_num);
        } else if (is_lett(c)) {
            cbuf[cbuf_n++] = c;
            need_new = true;
        } else {
            token_ready = true;
            t = Token(_error, 0, str_num);
        }
    }
}

void LexAnalysator::S_label1()
{
    need_new = true;
    if (is_lett(c)) {
        token_ready = false;
        cbuf[cbuf_n++] = c;
        state = s_label2;
    } else {
        token_ready = true;
        t = Token(_error, 0, str_num);
    }
}
void LexAnalysator::S_label2()
{
    if (cbuf_n == bufsz) {
        // too long name
        token_ready = true;
        t = Token(_error, 0, str_num);
    } else if (is_lett(c) || char_to_num(c) != -1) {
        need_new = true;
        cbuf[cbuf_n++] = c;
    } else if (is_incharset(SpaceDiv, c) ||
        is_incharset(NonSpaceDiv, c))
    {
        need_new = false;
        state = s_start;
        cbuf[cbuf_n++] = '\0';
        token_ready = true;
        int pr = tbl->add_label(cbuf);
        if (pr == -1) {
            t = Token(_error, 0, str_num);
        } else {
            t = Token(_label, pr, str_num);
        }
    } else {
        token_ready = true;
        t = Token(_error, 0, str_num);
    }
}


void LexAnalysator::S_start()
{
    token_ready = false;
    need_new = true;
    if (is_incharset(SpaceDiv, c)) {
        if (c == '\n')
            str_num++;
    } else if (is_lett(c)) {
        state = s_key;
        cbuf_n = 0;
        cbuf[cbuf_n++] = c;
    } else if (char_to_num(c) != -1) {
        state = s_integer;
        int_buf = c - '0';
    } else if (c == '"') {
        state = s_str;
        cbuf_n = 0;
    } else if (c == '@') {
        state = s_label1;
        cbuf_n = 0;
    } else if (c == '$') {
        state = s_id1;
        cbuf_n = 0;
    } else if (c == ':') {
        state = s_is;
    } else if (c == '!') {
        state = s_not;
    } else if (c == '<') {
        state = s_le;
    } else if (c == '>') {
        state = s_ge;
    } else {
        token_ready = true;
        int p;
        switch (c) {
        case ';': p = p_nl; break;
        case '{': p = p_ocb; break;
        case '}': p = p_ccb; break;
        case '(': p = p_osb; break;
        case ')': p = p_csb; break;
        case '+': p = p_plus; break;
        case '-': p = p_minus; break;
        case '/': p = p_div; break;
        case '*': p = p_mul; break;
        case '%': p = p_mod; break;
        case '&': p = p_and; break;
        case '|': p = p_or; break;
        case '=': p = p_eq; break;
        case ',': p = p_comma; break;
        case '[': p = p_oparr; break;
        case ']': p = p_clarr; break;
        default: t = Token(_error, 0, str_num); return;
        }
        t = Token(_serv, p, str_num);
    }
}
void LexAnalysator::S_str()
{
    need_new = true;
    if (c == '"') {
        token_ready = true;
        if (cbuf_n == 0) {
            //empty string
            t = Token(_error, 0, str_num);
        } else {
            state = s_start;
            cbuf[cbuf_n++] = '\0';
            int pr = tbl->add_str(cbuf);
            if (pr == -1) {
                //error no place in table
                t = Token(_error, 0, str_num);
            } else {
                t = Token(_str, pr, str_num);
            }
        }
    } else {
        if (cbuf_n == bufsz) {
            //error string too long;
            token_ready = true;
            t = Token(_error, 0, str_num);
        } else {
            cbuf[cbuf_n++] = c;
        }
    }
}

Token LexAnalysator::Foo()
{
    token_ready = false;
    switch (state) {
    case s_float: S_float(); break;
    case s_ge: S_ge(); break;
    case s_id1: S_id1(); break;
    case s_id2: S_id2(); break;
    case s_integer: S_integer(); break;
    case s_is: S_is(); break;
    case s_key: S_key(); break;
    case s_label1: S_label1(); break;
    case s_label2: S_label2(); break;
    case s_le: S_le(); break;
    case s_not: S_not(); break;
    case s_start: S_start(); break;
    case s_str: S_str(); break;
    default: return Token(_error, 0, str_num);
    }
    if (token_ready) {
        return t;
    } else {
        return Token(_notready, 0, str_num);
    }
}

Token LexAnalysator::GetToken0()
{
    if (need_new) {
        return Token(_neednew, 0, str_num);
    } else {
        return Foo();
    }
}

Token LexAnalysator::GetToken1(char cc)
{
    c = cc;
    return Foo();
}

bool LexAnalysator::Gt(char cc, Token &tkn)
{
//wrapper for GetToken0 and GetToken1 for common purposes
//if true then you should send new char next time
//else please send the same
    Token tmp = GetToken0();
    if (tmp.name == _neednew) {
        tkn = GetToken1(cc);
        return 1;
    } else {
        tkn = tmp;
        return 0;
    }
}
