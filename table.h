#ifndef __TABLE_H__
#define __TABLE_H__
#include "stupid_header.h"
#include "polish.h"
class SemanticError: public Error
{};
class DoubleDefError: public SemanticError
{
public:
    void PrintMsg();
};
class NotDefYetError: public SemanticError
{
public:
    void PrintMsg();
};
class NotSingleLabelJumpError
{
public:
    void PrintMsg();
};

enum {table_sz = 1024, bufsz = 128, max_f_point = 100000,
    start_f_point = 10};

enum TokenName
{
    _idm, _integer, _floating, _label, _str, _ida,
    _serv, _keywords, _onepar, _nopar, _error, _neednew, _notready,
    _end //_end is used only in syntaxanalyser
};

struct Token
{
    TokenName name;
    int i_table;
    int str_num;
    Token(TokenName tn = _end, int a = 0, int b = 0)
    : name(tn), i_table(a), str_num(b)
    {}
};

extern const char *SpaceDiv;
extern const char *NonSpaceDiv;

const String serv_table[] = {
//compare
"<", ">", "!=", "=", ">=", "<=",
//bool
"|", "&", "!",
//is
":=",
//;  and ,
";", "," ,
//brackets
")", "(", "}", "{",
//arithmetic
"+", "-", "*", "/", "%",
//arr_brackets
"[", "]",
""
};
enum {p_less, p_great, p_noteq, p_eq, p_geq, p_leq,
    p_or, p_and, p_notb, p_is, p_nl, p_comma, p_csb, p_osb, p_ccb,
    p_ocb, p_plus, p_minus, p_mul, p_div, p_mod, p_oparr, p_clarr,
};

const String keyw_table[] = {
//keywords
"goto", "while", "if", "else", "print",
//void-functions from botfunc.h
"buy", "sell", "prod", "build", "endturn",
//types
"int", "float",
""
};

enum {ista, rawprice, productprice, tturn, myid, pplayers, activeplayers,
    ssupply, ddemand, getid, costgood, costfact, costraw, unitprod, unitfact
};
const String func_nopar[] = {
"isTurnAvailable", "RawPrice", "ProductPrice", "Turn", "MyID", "Players",
"ActivePlayers", "Supply", "Demand", "GetID", "CostGood", "CostFact", "CostRaw",
"UnitProd", "UnitFact",
""
};

enum {mmoney, rraw, pproduction, ffactories, mmanuf, tradesprodbought,
    tradesprodprice, tradesrawbought, tradesrawprice
};
const String func_onepar[] = {
"Money", "Raw", "Production", "Factories", "Manufactured", "TradesProdBought",
"TradesProdPrice", "TradesRawBought", "TradesRawPrice",
""
};

struct Label
{
    String name;
    bool is_any_goto_label;
    bool is_only_one;
    PolishAtom **plabel;
    Label();
    bool operator==(const String &b) const
    {
        return name == b;
    }
    void operator=(const String &b)
    {
        name = b;
    }
};

enum {def_mint, def_aint, def_mfloat, def_afloat, def_notdef};

struct Payload
{
    int type;
    Payload(int tt)
    : type(tt)
    {}
    virtual ~Payload()
    {}
};

struct UserId
{
    String name;
    Payload *p;
    UserId()
    : name(0), p(0)
    {}
    ~UserId()
    {
        if (p)
            delete p;
    }
    bool operator==(const String &b) const
    {
        return name == b;
    }
    void operator=(const String &b)
    {
        name = b;
    }
};

struct IdmFloat: public Payload
{
    float f;
    IdmFloat(float init)
    : Payload(def_mfloat), f(init)
    {}
};

struct IdmInt: public Payload
{
    int i;
    IdmInt(int init)
    : Payload(def_mint), i(init)
    {}
};

struct IdaInt: public Payload
{
    int *p;
    int sz;
    IdaInt(int ssz)
    : Payload(def_aint)
    {
        p = new int [ssz];
        sz = ssz;
    }
    ~IdaInt()
    {
        delete [] p;
    }
};

struct IdaFloat: public Payload
{
    float *p;
    int sz;
    IdaFloat(int ssz)
    : Payload(def_afloat)
    {
        p = new float [ssz];
        sz = ssz;
    }
    ~IdaFloat()
    {
        delete [] p;
    }
};

struct Table
{
    int *int_table;
    int int_i;

    float *float_table;
    int float_i;

    Label *label_table;
    int label_i;

    String *str_table;
    int str_i;

    UserId *userid;
    int u_i;
    void define_userid(int i, int t, int asize = 0);
    void check_userid(int i);
    void define_goto_label(int i, PolishList &pl);
    void define_label_jump(int i, PolishList &pl);
    Table();
    ~Table();

    int add_int(int a);
    int add_label(String s);
    int add_str(String s);
    int add_userid(String s);
    int add_float(float a);
};
#endif
