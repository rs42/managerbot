#include "table.h"
const char *SpaceDiv = " \t\n\r";
const char *NonSpaceDiv = ";:(){}+-/*&=!|,<>%]";

void DoubleDefError::PrintMsg()
{
    printf("DoubleDefError ");
}

void NotDefYetError::PrintMsg()
{
    printf("NotDefYetError ");
}

void NotSingleLabelJumpError::PrintMsg()
{
    printf("NotSingleLabelJumpError ");
}


Label::Label()
: is_any_goto_label(0), is_only_one(0), plabel(0)
{}

void Table::define_userid(int i, int t, int asize)
{
    if (userid[i].p) {
        throw DoubleDefError();
    } else {
        switch (t) {
        case def_mfloat:
            userid[i].p = new IdmFloat(0);
            break;
        case def_mint:
            userid[i].p = new IdmInt(0);
            break;
        case def_afloat:
            userid[i].p = new IdaFloat(asize);
            break;
        default:
            userid[i].p = new IdaInt(asize);
            break;
        }
    }
}

void Table::check_userid(int i)
{
    if (!userid[i].p)
        throw NotDefYetError();
}
void Table::define_goto_label(int i, PolishList &pl)
{
    label_table[i].is_any_goto_label = 1;
    PolishLabel *d = new PolishLabel(&label_table[i].plabel);
    pl.add(d);
}
void Table::define_label_jump(int i, PolishList &pl)
{
    if (label_table[i].is_only_one == 0) {
        label_table[i].is_only_one = 1;
        label_table[i].plabel = pl.GetLast();
    } else {
        throw NotSingleLabelJumpError();
    }
}

Table::Table()
:int_i(0), float_i(0), label_i(0), str_i(0), u_i(0)
{
    int_table = new int [table_sz];
    float_table = new float [table_sz];
    label_table = new Label [table_sz];
    str_table = new String [table_sz];
    userid = new UserId [table_sz];
}
Table::~Table()
{
    delete [] int_table;
    delete [] float_table;
    delete [] label_table;
    delete [] str_table;
    delete [] userid;
}
template <class A, class B>
int add(B *arr, int &B_i, A x)
{
    for (int i = 0; i < B_i; i++) {
        if (arr[i] == x) {
            return i;
        }
    }
    if (B_i < table_sz) {
        arr[B_i++] = x;
        return B_i - 1;
    } else {
        return -1;
    }
}
int Table::add_float(float x)
{
    return add<float, float>(float_table, float_i, x);
}
int Table::add_int(int x)
{
    return add<int, int>(int_table, int_i, x);
}
int Table::add_label(String s)
{
    return add<String, Label>(label_table, label_i, s);
}
int Table::add_str(String s)
{
    return add<String, String>(str_table, str_i, s);
}
int Table::add_userid(String s)
{
    return add<String, UserId>(userid, u_i, s);
}
