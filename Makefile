CC = g++
OBJECTS = main.o lex.o parse.o syntax.o table.o polish.o strings.o botfunc.o
DFFLAGS = -Wall -c
ALLDEPS = main.cpp stupid_header.h lex.cpp lex.h syntax.cpp syntax.h table.cpp table.h polish.cpp polish.h strings.cpp strings.h botfunc.cpp botfunc.h parse.cpp parse.h

debug: DFFLAGS += -DDEBUG -g
debug: botandlang

botandlang: $(OBJECTS)
	$(CC) -g -o $@ $(OBJECTS)

.cpp.o: $(ALLDEPS)
	$(CC) $(DFFLAGS) $<

clean:
	rm -f $(OBJECTS) botandlang
