#ifndef __STRINGS_H__
#define __STRINGS_H__
#include "stupid_header.h"

struct String
{
    char *s;
    int sz;
    String(const char *str = 0);
    String(const String &b);
    bool operator==(const String &b) const;
    bool operator!=(const String &b) const;
    String operator+(const String &b) const;
    String &operator=(const String &b);
    int size() const;
    const char *GetStr() const;
    ~String()
    {
        if (s)
            delete [] s;
    }
    int send(int fd) const;
};
bool isspace2(const char &c);
#endif

