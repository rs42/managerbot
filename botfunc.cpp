#include "botfunc.h"
void GameInfo::GetServerInfo()
{
    iter = 0;
    try {
        //ww[0] ="#"
        month = atoi(ww[p_month].s);
        is_ta = atoi(ww[p_is_ta].s);
        bot_i = atoi(ww[p_bot_i].s);
        active_pl = atoi(ww[p_active_pl].s);
        max_pl = atoi(ww[p_max_pl].s);
        rw_q = atoi(ww[p_rw_q].s);
        rw_p = atoi(ww[p_rw_p].s);
        goods_q = atoi(ww[p_goods_q].s);
        goods_p = atoi(ww[p_goods_p].s);
        cost_good = atoi(ww[p_cost_good].s);
        cost_fact = atoi(ww[p_cost_fact].s);
        cost_raw = atoi(ww[p_cost_raw].s);
        unit_prod = atoi(ww[p_unit_prod].s);
        unit_fact = atoi(ww[p_unit_fact].s);
        //next ww[]  = "#" too
        if (plinfo)
            delete [] plinfo;
        plinfo = new PlayerInfo [active_pl];
        for (int i = 0, j = startoffset; i < active_pl; i++, j+=offset) {
            PlayerInfo ppi = {
                atoi(ww[j + id_].s),
                atoi(ww[j + money_].s),
                atoi(ww[j + rw_].s),
                atoi(ww[j + goods_].s),
                atoi(ww[j + factories_].s),
                atoi(ww[j + produced_].s),
                atoi(ww[j + sold_goods_q_].s),
                atoi(ww[j + sold_goods_p_].s),
                atoi(ww[j + bou_rw_q_].s),
                atoi(ww[j + bou_rw_p_].s)
                };
            plinfo[i] = ppi;
        }
    } catch (WordListEmptyError &) {
        printf("ERROR during parsing info from server\n");
        is_ta = 0;
    }
}

void GameInfo::buy(int quant, int price)
{
    dprintf(fdd, "buy %d %d\n", quant, price);
}
void GameInfo::sell(int quant, int price)
{
    dprintf(fdd, "sell %d %d\n", quant, price);
}
void GameInfo::prod(int q)
{
    dprintf(fdd, "prod %d\n", q);
}
void GameInfo::build(int q)
{
    dprintf(fdd, "build %d\n", q);
}
void GameInfo::endturn()
{
    dprintf(fdd, "ok\n");
    fd_set readfds;
    while (1) {
        FD_ZERO(&readfds);
        FD_SET(fdd, &readfds);
        if (select(fdd + 1, &readfds, 0, 0, 0) < 1) {
            perror("select");
            continue;
        }
        if (FD_ISSET(fdd, &readfds)) {
            do {
                ww.refresh(fdd);
                if (ww.is_closed) {
                    printf("Connection lost during game\n");
                    is_ta = 0;
                    return;
                } else if (ww.is_ready) {
                    if (ww[0] == "#") {
                        #ifdef DEBUG
                        printf("STATS from server\n");
                        #endif
                        GetServerInfo();
                        return;
                    } else {
                        #ifdef DEBUG
                        printf("MSG from server during game:\n");
                        #endif
                        ww.print();
                    }
                }
            } while (ww.is_buffered);
        }
    }
}

int GameInfo::is_turn_available()
{
    return is_ta;
}
int GameInfo::my_id()
{
    return bot_i;
}
int GameInfo::turn()
{
    return month;
}
int GameInfo::players()
{
    return max_pl;
}
int GameInfo::active_players()
{
    return active_pl;
}
int GameInfo::supply()
{
    return rw_q;
}
int GameInfo::raw_price()
{
    return rw_p;
}
int GameInfo::demand()
{
    return goods_q;
}
int GameInfo::production_price()
{
    return goods_p;
}
int GameInfo::money(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].money;
    }
    return 0;
}
int GameInfo::raw(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].rw;
    }
    return 0;

}
int GameInfo::production(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].goods;
    }
    return 0;
}
int GameInfo::factories(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].factories;
    }
    return 0;
}
int GameInfo::manufactured(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].produced;
    }
    return 0;
}
int GameInfo::result_prod_bought(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].sold_goods_q;
    }
    return 0;
}
int GameInfo::result_prod_price(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].sold_goods_p;
    }
    return 0;
}
int GameInfo::result_raw_bought(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].bou_rw_q;
    }
    return 0;
}
int GameInfo::result_raw_price(int id)
{
    for (int i = 0; i < active_pl; i++) {
        if (plinfo[i].id == id)
            return plinfo[i].bou_rw_p;
    }
    return 0;
}
int GameInfo::get_id()
{
    ++iter %= active_pl;
    return plinfo[iter].id;
}
int GameInfo::get_cost_good()
{
    return cost_good;
}
int GameInfo::get_cost_fact()
{
    return cost_fact;
}
int GameInfo::get_cost_raw()
{
    return cost_raw;
}
int GameInfo::get_unit_prod()
{
    return unit_prod;
}
int GameInfo::get_unit_fact()
{
    return unit_fact;
}
