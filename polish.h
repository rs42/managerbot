#ifndef __POLISH_H__
#define __POLISH_H__
#include "stupid_header.h"
#include "botfunc.h"
class PolishStack;
class PolishList;
class PolishError: public Error
{};
class DivByZero: public PolishError
{
public:
    void PrintMsg();
};
class ExpectedPolishTypeError: public PolishError
{
public:
    void PrintMsg();
};
class EmptyStackError: public PolishError
{
public:
    void PrintMsg();
};
class ArrayBordersError: public PolishError
{
public:
    void PrintMsg();
};


class PolishAtom
{
    friend class PolishList;
    PolishAtom *NextList;
public:
    virtual PolishAtom* Execute(PolishStack &polishstack, GameInfo &gi) = 0;
    virtual void PrintAtom() = 0;
    virtual ~PolishAtom()
    {}
    PolishAtom *GetNextList();
};

class PolishConst: public PolishAtom
{
    friend class PolishStack;
    PolishConst *NextStack;
public:
    PolishAtom* Execute(PolishStack &polishstack, GameInfo &gi);
};

class PolishList
{
    PolishAtom *start, *cur, **p;
    void Run(PolishStack &ps, GameInfo &gi);
public:
    PolishList();
    ~PolishList();
    void add(PolishAtom *a);
    template <class T>
    void add()
    {
        add(new T);
    }
    PolishAtom **GetLast();
    void PrintList()
    {
        cur = start;
        while (cur) {
            cur->PrintAtom();
            cur = cur->NextList;
        }
    }
    void RunPolish(int socket, WrapWL &ww);
};

class PolishNum: public PolishConst
{
public:
};

class PolishInt: public PolishNum
{
    int *x;
public:
    PolishInt(int *xx);
    int *Get();
    void PrintAtom()
    {
        printf("PolishInt\n");
    }
};

class PolishFloat: public PolishNum
{
    float *x;
public:
    PolishFloat(float *xx);
    float *Get();
    void PrintAtom()
    {
        printf("PolishFloat\n");
    }
};


class PolishIntTmp: public PolishInt
{
    bool is_del;
public:
    PolishIntTmp(int *xx, bool i_is_del);
    ~PolishIntTmp();
    PolishIntTmp(PolishIntTmp& a);
};

class PolishFloatTmp: public PolishFloat
{
    bool is_del;
public:
    PolishFloatTmp(float *xx, bool i_is_del);
    ~PolishFloatTmp();
    PolishFloatTmp(PolishFloatTmp& a);
};

class PolishStack
{
    PolishConst *top, *tmp;
public:
    PolishStack();
    void Push(PolishConst* pc);
    void Pop();
    PolishConst* GetTop();
};

class PolishString: public PolishConst
{
    const String *s;
public:
    PolishString(const String *ss)
    : s(ss)
    {}
    String GetStr();
    void PrintAtom()
    {
        printf("PolishString: %s\n", (*s).s);
    }
};

class PolishLabel: public PolishConst
{
protected:
    PolishAtom ***labelto;
public:
    PolishLabel(PolishAtom ***k)
    : labelto(k)
    {}
    PolishAtom *GetLabel();
    void PrintAtom()
    {
        printf("PolishLabel\n");
    }
};
class PolishLabelSD: public PolishLabel
{
public:
    PolishLabelSD(PolishAtom ***k)
    : PolishLabel(k)
    {}
    ~PolishLabelSD()
    {
        if (labelto)
            delete labelto;
    }
};

class PolishFunc: public PolishAtom
{
    virtual void ExecuteFunc(PolishStack &polishstack, GameInfo &gi) = 0;
public:
    PolishAtom *Execute(PolishStack &polishstack, GameInfo &gi);
};

class PolishIs: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
    void PrintAtom()
    {
        printf("PolishIs\n");
    }
};

class PolishGoto: public PolishAtom
{
    virtual bool is_cond(PolishStack &polishstack);
public:
    PolishAtom* Execute(PolishStack &polishstack, GameInfo &gi);
    void PrintAtom()
    {
        printf("PolishGoto\n");
    }
};

class PolishGotoFalse: public PolishGoto
{
    bool is_cond(PolishStack &polishstack);
public:
    void PrintAtom()
    {
        printf("PolishGotoFalse\n");
    }
};

class PolishStop: public PolishConst
{
public:
    void PrintAtom()
    {
        printf("PolishStop\n");
    }
};
class PolishPrint: public PolishFunc
{
    void Print(PolishStack &polishstack);
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
public:
    void PrintAtom()
    {
        printf("PolishPrint\n");
    }
};

class PolishBoolFunc: public PolishFunc
{
    virtual int magic(int a, int b) = 0;
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
};
class PolishOr: public PolishBoolFunc
{
    int magic(int a, int b);
public:
    void PrintAtom()
    {
        printf("PolishOr\n");
    }
};
class PolishAnd: public PolishBoolFunc
{
    int magic(int a, int b);
public:
    void PrintAtom()
    {
        printf("PolishAnd\n");
    }
};

class PolishArith: public PolishFunc
{
    virtual float f_magic(float a, float b) = 0;
    virtual int i_magic(int a, int b) = 0;
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
};
class PolishSum: public PolishArith
{
    int i_magic(int a, int b);
    float f_magic(float a, float b);
public:
    void PrintAtom()
    {
        printf("PolishSum\n");
    }
};
class PolishDiv: public PolishArith
{
    int i_magic(int a, int b);
    float f_magic(float a, float b);
public:
    void PrintAtom()
    {
        printf("PolishDiv\n");
    }
};
class PolishMod: public PolishArith
{
    int i_magic(int a, int b);
    float f_magic(float a, float b);
public:
    void PrintAtom()
    {
        printf("PolizMod\n");
    }
};
class PolishMul: public PolishArith
{
    int i_magic(int a, int b);
    float f_magic(float a, float b);
public:
    void PrintAtom()
    {
        printf("PolishMul\n");
    }
};
class PolishSub: public PolishArith
{
    int i_magic(int a, int b);
    float f_magic(float a, float b);
public:
    void PrintAtom()
    {
        printf("PolishMul\n");
    }
};
class PolishCompare: public PolishFunc
{
    virtual int f_magic(float a, float b) = 0;
    virtual int i_magic(int a, int b) = 0;
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
};
class PolishNotEqual: public PolishCompare
{
    int f_magic(float a, float b);
    int i_magic(int a, int b);
public:
    void PrintAtom()
    {
        printf("PolishNotEqual\n");
    }
};
class PolishEqual: public PolishCompare
{
    int f_magic(float a, float b);
    int i_magic(int a, int b);
public:
    void PrintAtom()
    {
        printf("PolishEqual\n");
    }
};
class PolishGreater: public PolishCompare
{
    int f_magic(float a, float b);
    int i_magic(int a, int b);
public:
    void PrintAtom()
    {
        printf("PolishGreater\n");
    }
};
class PolishGreaterEq: public PolishCompare
{
    int f_magic(float a, float b);
    int i_magic(int a, int b);
public:
    void PrintAtom()
    {
        printf("PolishGreaterEq\n");
    }
};
class PolishLess: public PolishCompare
{
    int f_magic(float a, float b);
    int i_magic(int a, int b);
public:
    void PrintAtom()
    {
        printf("PolishLess\n");
    }
};
class PolishLessEq: public PolishCompare
{
    int f_magic(float a, float b);
    int i_magic(int a, int b);
public:
    void PrintAtom()
    {
        printf("PolishLessEq\n");
    }
};

class PolishNot: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
public:
    void PrintAtom()
    {
        printf("PolishNot\n");
    }
};
class PolishMinus: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
public:
    void PrintAtom()
    {
        printf("PolishMinus\n");
    }
};

class PolishBrackets: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
public:
    void PrintAtom()
    {
        printf("PolishBrackets\n");
    }
};

class PolishEndturn: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
    {
        gi.endturn();
    }
public:
    void PrintAtom()
    {
        printf("PolishEndturn\n");
    }
};

class PolishBuy: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
public:
    void PrintAtom()
    {
        printf("PolishBuy\n");
    }
};

class PolishSell: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
public:
    void PrintAtom()
    {
        printf("PolishSell\n");
    }
};

class PolishBuild: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
public:
    void PrintAtom()
    {
        printf("PolishBuild\n");
    }
};

class PolishProd: public PolishFunc
{
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
public:
    void PrintAtom()
    {
        printf("PolishProd\n");
    }
};

class PolishNoArgFuncRet: public PolishFunc
{
    virtual int botf(GameInfo &gi) = 0;
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
    {
        polishstack.Push(new PolishIntTmp(new int (botf(gi)), true));
    }
};
class PolishIsTurnAvailable: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.is_turn_available();
    }
public:
    void PrintAtom()
    {
        printf("PolishIsTurnAvailable\n");
    }
};
class PolishRawPrice: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.raw_price();
    }
public:
    void PrintAtom()
    {
        printf("PolishRawPrice\n");
    }
};
class PolishProductPrice: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.production_price();
    }
public:
    void PrintAtom()
    {
        printf("PolishProductPrice\n");
    }
};
class PolishTurn: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.turn();
    }
public:
    void PrintAtom()
    {
        printf("PolishTurn\n");
    }
};
class PolishMyID: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.my_id();
    }
public:
    void PrintAtom()
    {
        printf("PolishMyID\n");
    }
};
class PolishPlayers: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.players();
    }
public:
    void PrintAtom()
    {
        printf("PolishPlayers\n");
    }
};
class PolishActivePlayers: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.active_players();
    }
public:
    void PrintAtom()
    {
        printf("PolishActivePlayers\n");
    }
};
class PolishSupply: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.supply();
    }
public:
    void PrintAtom()
    {
        printf("PolishSupply\n");
    }
};
class PolishDemand: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.demand();
    }
public:
    void PrintAtom()
    {
        printf("PolishDemand\n");
    }
};
class PolishGetID: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.get_id();
    }
public:
    void PrintAtom()
    {
        printf("PolishGetID\n");
    }
};
class PolishUnitFact: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.get_unit_fact();
    }
public:
    void PrintAtom()
    {
        printf("PolishUnitFact\n");
    }
};
class PolishUnitProd: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.get_unit_prod();
    }
public:
    void PrintAtom()
    {
        printf("PolishUnitProd\n");
    }
};
class PolishCostRaw: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.get_cost_raw();
    }
public:
    void PrintAtom()
    {
        printf("PolishCostRaw\n");
    }
};
class PolishCostFact: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.get_cost_fact();
    }
public:
    void PrintAtom()
    {
        printf("PolishCostFact\n");
    }
};
class PolishCostGood: public PolishNoArgFuncRet
{
    int botf(GameInfo &gi)
    {
        return gi.get_cost_good();
    }
public:
    void PrintAtom()
    {
        printf("PolishCostGood\n");
    }
};

class PolishOneArgFuncRet: public PolishFunc
{
    virtual int botf(int arg, GameInfo &gi) = 0;
    void ExecuteFunc(PolishStack &polishstack, GameInfo &gi);
};
class PolishMoney: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.money(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishMoney\n");
    }
};
class PolishRaw: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.raw(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishRaw\n");
    }
};
class PolishProduction: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.production(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishProduction\n");
    }
};
class PolishFactories: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.factories(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishFactories\n");
    }
};
class PolishManufactured: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.manufactured(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishManufactured\n");
    }
};
class PolishTradesProdBought: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.result_prod_bought(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishTradesProdBought\n");
    }
};
class PolishTradesProdPrice: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.result_prod_price(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishTradesProdPrice\n");
    }
};
class PolishTradesRawBought: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.result_raw_bought(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishTradesRawBought\n");
    }
};
class PolishTradesRawPrice: public PolishOneArgFuncRet
{
    int botf(int arg, GameInfo &gi)
    {
        return gi.result_raw_price(arg);
    }
public:
    void PrintAtom()
    {
        printf("PolishTradesRawPrice\n");
    }
};
#endif
