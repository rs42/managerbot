#ifndef __ST_HEADER_H__
#define __ST_HEADER_H__
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <signal.h>
#include <fcntl.h>
#include <stdarg.h>
#include "strings.h"
class Error
{
public:
    virtual void PrintMsg() = 0;
};
#endif
