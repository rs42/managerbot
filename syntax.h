#ifndef __SYNTAX_H__
#define __SYNTAX_H__
#include "stupid_header.h"
#include "table.h"
#include "lex.h"
#include "polish.h"
class LexError: public Error
{
public:
    void PrintMsg();
};
class SyntaxError: public Error
{};
class ExprError: public SyntaxError
{
public:
    void PrintMsg();
};
class GarbageSymError: public SyntaxError
{
public:
    void PrintMsg();
};
class MatchNameError: public SyntaxError
{
public:
    void PrintMsg();
};
class MatchStrError: public SyntaxError
{
public:
    void PrintMsg();
};


class SyntaxAnalysator
{
    char c;
    Token tkn;
    Table &tbl;
    LexAnalysator la;
    void NextToken();
    void MatchName(TokenName tn);
    void MatchStr(const String &s);
    bool CheckName(TokenName tn);
    bool CheckStr(const String &s);
    void addInt();
    void addFloat();
    void addString();
    void addUserID();
    void addNopar();
    void addOnepar();
    PolishList &polishlist;
    void S();
    void Expr();
    void A();
    void B();
    void C();
    void D();
    void E();
    void F();
    void G();
    void H();
    void S_goto();
    void S_if();
    void S_while();
    void S_id_def_fun(int dt);
    void S_id_def();
    void S_print();
    void S_is();
    int scriptfd;
public:
    SyntaxAnalysator(int sscriptfd, Table &t, PolishList &pl);
    ~SyntaxAnalysator();
    int StartSyntAn();
};
#endif
