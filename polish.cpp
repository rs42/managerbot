#include "polish.h"
void DivByZero::PrintMsg()
{
    printf("DivByZero ");
}

void ExpectedPolishTypeError::PrintMsg()
{
    printf("ExpectedPolishTypeError ");
}

void EmptyStackError::PrintMsg()
{
    printf("EmptyStackError ");
}
void ArrayBordersError::PrintMsg()
{
    printf("Out of array borders ");
}
PolishAtom * PolishAtom::GetNextList()
{
    return NextList;
}

PolishList::PolishList()
: start(0), cur(start), p(&start)
{}
PolishList::~PolishList()
{
    while (start) {
        cur = start;
        start = start->NextList;
        delete cur;
    }
}
void PolishList::add(PolishAtom *a)
{
    *p = a;
    a->NextList = 0;
    p = &(a->NextList);
}
PolishAtom** PolishList::GetLast()
{
    return p;
}

void PolishList::Run(PolishStack &ps, GameInfo &gi)
{
    cur = start;
    while (cur)
        cur = cur->Execute(ps, gi);
}
PolishInt::PolishInt(int *xx)
: x(xx)
{}
int *PolishInt::Get()
{
    return x;
}

PolishFloat::PolishFloat(float *xx)
: x(xx)
{}
float *PolishFloat::Get()
{
    return x;
}

PolishIntTmp::PolishIntTmp(int *xx, bool i_is_del)
: PolishInt(xx), is_del(i_is_del)
{}
PolishIntTmp::~PolishIntTmp()
{
    if (is_del) {
        delete Get();
    }
}
PolishIntTmp::PolishIntTmp(PolishIntTmp& a)
: PolishInt(new int(*(a.Get()))), is_del(true)
{}


PolishFloatTmp::PolishFloatTmp(float *xx, bool i_is_del)
: PolishFloat(xx), is_del(i_is_del)
{}
PolishFloatTmp::~PolishFloatTmp()
{
    if (is_del) {
        delete Get();
    }
}
PolishFloatTmp::PolishFloatTmp(PolishFloatTmp& a)
: PolishFloat(new float(*a.Get())), is_del(true)
{}

PolishStack::PolishStack()
: top(0)
{}
PolishConst* PolishStack::GetTop()
{
    if (top)
        return top;
    else
        throw EmptyStackError();
}

void PolishStack::Pop()
{
    if (top) {
        tmp = top;
        top = top->NextStack;
        if (dynamic_cast<PolishIntTmp*>(tmp) ||
            dynamic_cast<PolishFloatTmp*>(tmp))
        {
            delete tmp;
        }
    } else {
        throw EmptyStackError();
    }
}
void PolishStack::Push(PolishConst* pc)
{
    pc->NextStack = top;
    top = pc;
}

PolishAtom* PolishConst::Execute(PolishStack &polishstack, GameInfo &gi)
{
    polishstack.Push(this);
    return GetNextList();
}

String PolishString::GetStr()
{
    return *s;
}

PolishAtom *PolishLabel::GetLabel()
{
    return **labelto;
}

bool int_or_float(PolishConst *pc, int &a, float &b)
{
    PolishInt *kekInt;
    PolishFloat *kekFloat;
    if ((kekInt = dynamic_cast<PolishInt*>(pc))) {
        a = *(kekInt->Get());
        return 1;
    } else if ((kekFloat = dynamic_cast<PolishFloat*>(pc))) {
        b = *(kekFloat->Get());
        return 0;
    } else {
        throw ExpectedPolishTypeError();
    }
}


PolishAtom *PolishFunc::Execute(PolishStack &polishstack, GameInfo &gi)
{
    ExecuteFunc(polishstack, gi);
    return GetNextList();
}

bool PolishGoto::is_cond(PolishStack &polishstack)
{
    return 1;
}
PolishAtom* PolishGoto::Execute(PolishStack &polishstack, GameInfo &gi)
{
    if (is_cond(polishstack)) {
        PolishLabel *pl = dynamic_cast<PolishLabel*>(polishstack.GetTop());
        PolishAtom *res = pl->GetLabel();
        polishstack.Pop();
        return res;
    }
    polishstack.Pop();
    return GetNextList();
}

bool PolishGotoFalse::is_cond(PolishStack &polishstack)
{
    int i;
    float f;
    if (int_or_float(polishstack.GetTop(), i, f)) {
        polishstack.Pop();
        return !i;
    } else {
        polishstack.Pop();
        return 0;
    }
}

void PolishPrint::Print(PolishStack &polishstack)
{
    PolishConst *arg;
    PolishString *ps;
    while (1) {
        try {
            arg = polishstack.GetTop();
        } catch (EmptyStackError&) {
            printf("\n");
            return;
        }
        if ((ps = dynamic_cast<PolishString*>(arg))) {
            printf("%s", ps->GetStr().s);
        } else {
            int a;
            float b;
            if (int_or_float(arg, a, b)) {
                printf("%d", a);
            } else {
                printf("%f", b);
            }
        }
            polishstack.Pop();
    }
}
void PolishPrint::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    PolishStack rev;
    PolishConst *arg = polishstack.GetTop();
    PolishIntTmp *it, *ii;
    PolishFloatTmp *ft, *ff;
    while (!dynamic_cast<PolishStop*>(arg)) {
        if ((it = dynamic_cast<PolishIntTmp*>(arg))) {
            ii = new PolishIntTmp(*it);
            rev.Push(ii);
            polishstack.Pop();
        } else if ((ft = dynamic_cast<PolishFloatTmp*>(arg))) {
            ff = new PolishFloatTmp(*ft);
            rev.Push(ff);
            polishstack.Pop();
        } else {
            polishstack.Pop();
            rev.Push(arg);
        }
        arg = polishstack.GetTop();
    }
    polishstack.Pop();
    Print(rev);
}

void PolishBoolFunc::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    PolishConst *arg = polishstack.GetTop();
    int  a, b;
    float f;
    if (int_or_float(arg, a, f)) {
        polishstack.Pop();
        arg = polishstack.GetTop();
        if (int_or_float(arg, b, f)) {
        } else {
            a = b = 1;
        }
        polishstack.Pop();
    } else {
        a = b = 1;
        polishstack.Pop();
        polishstack.Pop();
    }
    int *r = new int(magic(a, b));
    PolishIntTmp *res = new PolishIntTmp(r, true);
    polishstack.Push(res);
}
int PolishOr::magic(int a, int b)
{
    return a|b;
}

int PolishAnd::magic(int a, int b)
{
    return a&&b;
}

void PolishArith::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    PolishConst *arg = polishstack.GetTop(), *res;
    int  ia, ib;
    float fa, fb;
    int ccase = int_or_float(arg, ia, fa);
    polishstack.Pop();
    arg = polishstack.GetTop();
    ccase = (ccase << 1) | int_or_float(arg, ib, fb);
    switch (ccase) {
    case 0: {
        float *r = new float (f_magic(fa, fb));
        res = new PolishFloatTmp(r, true);
        break; }
    case 1: {
        float *r = new float (f_magic(fa, ib));
        res = new PolishFloatTmp(r, true);
        break; }
    case 2: {
        float *r = new float (f_magic(ia, fb));
        res = new PolishFloatTmp(r, true);
        break; }
    default: {
        int *r = new int (i_magic(ia, ib));
        res = new PolishIntTmp(r, true);
        break; }
    }
    polishstack.Pop();
    polishstack.Push(res);
}

int PolishSum::i_magic(int a, int b)
{
    return b+a;
}
float PolishSum::f_magic(float a, float b)
{
    return b+a;
}
int PolishDiv::i_magic(int a, int b)
{
    if (a)
        return b/a;
    throw DivByZero();
}
float PolishDiv::f_magic(float a, float b)
{
    if (a)
        return b/a;
    throw DivByZero();
}

int PolishMod::i_magic(int a, int b)
{
    if (a)
        return b%a;
    throw DivByZero();
}
float PolishMod::f_magic(float a, float b)
{
    if ((int)a)
        return ((int)b % (int)a) + b - (int)b;
    throw DivByZero();
}

int PolishMul::i_magic(int a, int b)
{
    return b*a;
}
float PolishMul::f_magic(float a, float b)
{
    return b*a;
}

int PolishSub::i_magic(int a, int b)
{
    return b-a;
}
float PolishSub::f_magic(float a, float b)
{
    return b-a;
}

void PolishCompare::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    PolishConst *arg = polishstack.GetTop(), *res;
    int  ia, ib;
    float fa, fb;
    int *r;
    int ccase = int_or_float(arg, ia, fa);
    polishstack.Pop();
    arg = polishstack.GetTop();
    ccase = (ccase << 1) | int_or_float(arg, ib, fb);
    switch (ccase) {
    case 0:
        r = new int (f_magic(fa, fb));
        break;
    case 1:
        r = new int (f_magic(fa, ib));
        break;
    case 2:
        r = new int (f_magic(ia, fb));
        break;
    default:
        r = new int (i_magic(ia, ib));
        break;
    }
    res = new PolishIntTmp(r, true);
    polishstack.Pop();
    polishstack.Push(res);
}


int PolishNotEqual::f_magic(float a, float b)
{
    return a != b;
}
int PolishNotEqual::i_magic(int a, int b)
{
    return a != b;
}

int PolishEqual::f_magic(float a, float b)
{
    return a == b;
}
int PolishEqual::i_magic(int a, int b)
{
    return a == b;
}

int PolishGreater::f_magic(float a, float b)
{
    return b > a;
}
int PolishGreater::i_magic(int a, int b)
{
    return b > a;
}

int PolishGreaterEq::f_magic(float a, float b)
{
    return b >= a;
}
int PolishGreaterEq::i_magic(int a, int b)
{
    return b >= a;
}

int PolishLess::f_magic(float a, float b)
{
    return b < a;
}
int PolishLess::i_magic(int a, int b)
{
    return b < a;
}

int PolishLessEq::f_magic(float a, float b)
{
    return b <= a;
}
int PolishLessEq::i_magic(int a, int b)
{
    return b <= a;
}

void PolishNot::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    PolishConst *arg = polishstack.GetTop();
    int i;
    float f;
    if (int_or_float(arg, i, f))
        i = !i;
    else
        i = 0;
    int *r = new int(i);
    PolishConst *res = new PolishIntTmp(r, true);
    polishstack.Pop();
    polishstack.Push(res);
}

void PolishMinus::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    PolishConst *arg = polishstack.GetTop();
    int i;
    float f;
    if (int_or_float(arg, i, f)) {
        int *r = new int(-i);
        PolishConst *res = new PolishIntTmp(r, true);
        polishstack.Pop();
        polishstack.Push(res);
    } else {
        float *r = new float(-f);
        PolishConst *res = new PolishFloatTmp(r, true);
        polishstack.Pop();
        polishstack.Push(res);
    }
}


bool int_or_float_addr(PolishConst *pc, int* &a, float* &b)
{
    PolishInt *kekInt;
    PolishFloat *kekFloat;
    if ((kekInt = dynamic_cast<PolishInt*>(pc))) {
        a = kekInt->Get();
        return 1;
    } else if ((kekFloat = dynamic_cast<PolishFloat*>(pc))) {
        b = kekFloat->Get();
        return 0;
    } else {
        throw ExpectedPolishTypeError();
    }
}

void PolishIs::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    int *i_thing, i_expr;
    float *f_thing, f_expr;
    PolishConst *arg = polishstack.GetTop();
    int ccase = int_or_float(arg, i_expr, f_expr);
    polishstack.Pop();
    arg = polishstack.GetTop();
    ccase = (ccase << 1) | int_or_float_addr(arg, i_thing, f_thing);

    switch (ccase) {
    case 0:
        *f_thing = f_expr;
        break;
    case 1:
        *i_thing = f_expr;
        break;
    case 2:
        *f_thing = i_expr;
        break;
    case 3:
        *i_thing = i_expr;
        break;
    default:
        break;
    }
    polishstack.Pop();
}

void PolishBrackets::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    int index, *int_arr, a, sz;
    float *float_arr, b;
    PolishConst *arg = polishstack.GetTop(), *res;
    index = (int_or_float(arg, a, b)) ? a : b;
    polishstack.Pop();
    arg = polishstack.GetTop();
    sz = (int_or_float(arg, a, b)) ? a : b;
    if (index >= sz || index < 0)
        throw ArrayBordersError();
    polishstack.Pop();
    arg = polishstack.GetTop();
    if (int_or_float_addr(arg, int_arr, float_arr))
        res = new PolishIntTmp(int_arr + index, false);
    else
        res = new PolishFloatTmp(float_arr + index, false);
    polishstack.Pop();
    polishstack.Push(res);
}

int get_arg(PolishStack &polishstack)
{
    PolishConst *arg = polishstack.GetTop();
    int a; float b;
    int res = (int_or_float(arg, a, b)) ? a : b;
    polishstack.Pop();
    return res;
}

void PolishBuy::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    int arg2 = get_arg(polishstack);
    int arg1 = get_arg(polishstack);
    gi.buy(arg1, arg2);
}

void PolishSell::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    int arg2 = get_arg(polishstack);
    int arg1 = get_arg(polishstack);
    gi.sell(arg1, arg2);
}
void PolishBuild::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    int arg = get_arg(polishstack);
    gi.build(arg);
}
void PolishProd::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    int arg = get_arg(polishstack);
    gi.prod(arg);
}

void PolishOneArgFuncRet::ExecuteFunc(PolishStack &polishstack, GameInfo &gi)
{
    PolishConst *argc = polishstack.GetTop();
    int a; float b;
    int arg = (int_or_float(argc, a, b)) ? a : b;
    polishstack.Pop();
    polishstack.Push(new PolishIntTmp(new int (botf(arg, gi)), true));
}

void PolishList::RunPolish(int socket, WrapWL &ww)
{
    GameInfo gi(socket, ww);
    while (ww.is_buffered) {
        ww.refresh(socket);
        if (ww.is_closed) {
            printf("Connection lost during game\n");
            return;
        } else if (ww.is_ready) {
            if (ww[0] == "#") {
                #ifdef DEBUG
                printf("Stats from server\n");
                #endif
                gi.GetServerInfo();
                goto ready;
            } else {
                #ifdef DEBUG
                printf("Received from server during game:\n");
                #endif
                ww.print();
            }
        }
    }
    fd_set readfds;
    while (1) {
        FD_ZERO(&readfds);
        FD_SET(socket, &readfds);
        if (select(socket + 1, &readfds, 0, 0, 0) < 1) {
            perror("select");
            continue;
        }
        if (FD_ISSET(socket, &readfds)) {
            do {
                ww.refresh(socket);
                if (ww.is_closed) {
                    printf("connection lost during game\n");
                    return;
                } else if (ww.is_ready) {
                    if (ww[0] == "#") {
                        #ifdef DEBUG
                        printf("Stats from server\n");
                        #endif
                        gi.GetServerInfo();
                        goto ready;
                    } else {
                        #ifdef DEBUG
                        printf("Received from server during game:\n");
                        #endif
                        ww.print();
                    }
                }
            } while (ww.is_buffered);
        }
    }
    ready:
    PolishStack polishstack;
    try {
        Run(polishstack, gi);
    } catch (PolishError &e) {
        e.PrintMsg();
        printf("\n");
    }
}

